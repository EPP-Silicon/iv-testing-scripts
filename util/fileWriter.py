import os
from . import pullEnv 
import datetime

"""
Function to collect metadata required for IV measurement header
"""
class fileWriter():

   def __init__(self):
      self.file = None
      self.fileName = None
      self.moduleType = None
      self.temperatureValue = None

      self.batchNum = None
      self.waferNum = None
      self.runNum = None
      self.sensorType = None
      self.serialNum = None
      self.comments = None


   def getFileReady(self, runType):
      self.collectRunInfo()
      self.setFileName(runType)
      self.openFile()
      self.writeRunInfo(runType)

   def setFileName(self, runType):
  
      if runType == 'sensor': 
         self.fileName = "{}_W{}_IV_{}.dat".format(self.batchNum, self.waferNum, self.runNum)
      if runType == 'tabbedSensor': 
         self.fileName = "{}_HV_TABBED_ATTACHED_IV_{}.dat".format(self.serialNum, self.runNum)
      if runType == 'module': 
         self.fileName = "{}_BONDED_IV_{}.dat".format(self.serialNum, self.runNum)
      if runType == 'test':
         self.fileName = "Test file {}".format(self.comments)

   def openFile(self):
      self.file = open('results/{}'.format(self.fileName), 'w')

   def writeRunInfo(self,runType):
      self.temperatureValue, humidityValue = pullEnv.queryDB()

      self.file.write("{}\n".format(self.fileName))
      self.file.write("Type: {}\n".format(self.sensorType))
      self.file.write("Batch: {}\n".format(self.batchNum))
      self.file.write("Wafer: {}\n".format(self.waferNum))
      self.file.write("Component: {}\n".format(self.serialNum))
      self.file.write("Date: {}\n".format(datetime.datetime.now().strftime("%d %b %Y")))
      self.file.write("Time: {}\n".format(datetime.datetime.now().strftime("%H:%M:%S")))
      self.file.write("Institute: MELB\n")
      self.file.write("TestType: {}_IV_TEST_V1\n".format(self.sensorType[:7])) #Does the test iteration 'V1' ever change?
      self.file.write("Vbias_SMU: KEITHLEY_2260B_800_1\n")
      self.file.write("Rseries: 1.0 MOhm\n")
      self.file.write("Test_DMM: none\n")
      self.file.write("Rshunt: 0.0 MOhm\n")
      self.file.write("RunNumber: {}\n".format(self.runNum[2]))
      self.file.write("Temperature: {:.1f}\n".format(self.temperatureValue))
      self.file.write("Humidity: {:.1f}\n".format(humidityValue))
      self.file.write("Comments: {}\n".format(self.comments))
      self.file.write("Voltage[V] Current [nA] Shunt_voltage[mV]\n")


   def collectRunInfo(self):
      """
      Establish sensor under test info.
      """

      while True:
         self.batchNum = input('Enter sensor batch number, eg. VPX32411: ')
         if(len(self.batchNum) == 8):
            break
         print('Please enter a valid batch number')

      while True:
         self.waferNum = input('Enter five digit sensor wafer number, eg. 00036: ')
         if(len(self.waferNum) == 5):
            break
         print('Please enter a valid wafer number')

      while True:
         self.runNum = input('Enter three digit run number, eg. 001: ')
         if(os.path.isfile("{}_W{}_IV_{}.dat".format(self.batchNum, self.waferNum, self.runNum))):
            print('{}_W{}_IV_{}.dat already exists, please increment run number'.format(self.batchNum, self.waferNum, self.runNum))
            continue
         if(len(self.runNum) == 3):
            break
         print('Please enter a valid run number')

      while True:
         self.sensorType = input('Enter nine digit sensor type, eg. ATLAS18LS: ')
         if(len(self.sensorType) == 9):
            break
         print('Please enter a valid sensor type')

      while True:
         self.serialNum = input('Enter 14 digit database serial number, eg. 20USBSL0000036: ')
         if(len(self.serialNum) == 14):
            break
         print('Please enter a valid serial number')

      while True:
         self.moduleType = input('Enter module type, eg. R1: ')
         if(len(self.moduleType) == 2):
            break
         print('Please enter a valid module type')

      self.comments = input('Additional comments realting to the current test... ')

   def getModuleType(self):
      return self.moduleType   
   def getTemperature(self):
      return self.temperatureValue

   def addMeasurementToFile(self, voltage, current):
      #convert current into nA
      current = float(current) * 1E9
      self.file.write("{:010.2f} {:010.2f} 000000.00\n".format(voltage, current)) #tabbed or spaced?

   def closeFile(self):
      self.file.close()
