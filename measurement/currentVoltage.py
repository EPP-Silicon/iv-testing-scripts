import sys
import numpy
import time

from util.Keithley2260B import Keithley2260B
from util.Keithley6487 import Keithley6487
from util.fileWriter import fileWriter

def performIV(runType, config):

    outputFile = fileWriter()
    outputFile.getFileReady(runType)
    thresholdCurrent = 1E-10

    #calculate current threshold for ending run
    if outputFile.getModuleType() == "R1":
        thresholdCurrent = normaliseCurrent(8.906E-6,outputFile.getTemperature())
    elif outputFile.getModuleType() == "R4":
        thresholdCurrent = normaliseCurrent(8.745E-6,outputFile.getTemperature())
    elif outputFile.getModuleType() == "R0": #Include for prototype testing
        thresholdCurrent = normaliseCurrent(8.99E-6,outputFile.getTemperature())        
    else:
        print('Could not configure a safe current threshold, closing program ...')
        sys.exit(1)

    #load run parameters and print to user for confirmation

    print('Loading the following run parameters for device type: ' + str(runType))
    print('Final voltage {} V'.format(config['voltageFinal']))
    print('Voltage step size {} V'.format(config['stepUpSize']))
    print('Time between measurements {} s'.format(config['stepUpWaitTime']))
    print('Ramp down voltage increments {} V'.format(config['stepDownSize']))
    print('Ramp down step time {} s'.format(config['stepDownWaitTime']))
    print('Number of stability measurements to be taken ' + str(config['stabilityMeasurements']))
    print('Threshold current set to {} A'.format(thresholdCurrent))
    print('If these settings are correct type \'yes\' to continue, otherwise \'quit\'')

    while True:
        response = input()
        if response == '':
            continue
        elif response == 'quit':
            sys.exit(0)
        elif response == 'yes':
            break
        else:
            print('Please enter \'yes\' or \'quit\'')
            continue
    
    #set up devices and output

    ps = Keithley2260B()
    ps.setupDevice()
    ammeter = Keithley6487()
    ammeter.setupDevice()

#    outputFile.getFileReady(runType)

    print('Finished with set up, beginning measurement.')

#    numOfSteps = config['voltageFinal'] / config['stepSize']
    print('Turning PSU on')
    ps.powerState('on')

    finalVoltage = None

    try:
        for i in range(0,config['voltageFinal']+1,config['stepUpSize']):

            ps.setVoltage(i)
            finalVoltage = i
            time.sleep(config['stepUpWaitTime'])

            current = ammeter.readCurrent()
            print('Voltage {}, current {}'.format(i, current))

            outputFile.addMeasurementToFile(i,current)
    #        print(current,thresholdCurrent)
            if float(current) > float(thresholdCurrent):
                print('Current exceeds {}, ramping bias voltage down.'.format(thresholdCurrent))            
                break
            if i == config['voltageFinal']:
                for j in range(0,config['stabilityMeasurements']+1):
                    time.sleep(10)
                    current = ammeter.readCurrent()
                    print('Stability measurement {} ... \n Voltage {}, current {}'.format(j, i, current))
                    outputFile.addMeasurementToFile(i,current)
    except KeyboardInterrupt:
        print('Manual abort initiated...')
        pass

    print('Voltage stepping down')
    for i in range(finalVoltage,0,-1*config['stepDownSize']):
        ps.setVoltage(i)
        print('Voltage: {}'.format(i))
        time.sleep(config['stepDownWaitTime'])

    print('Power supply powering off...')
    ps.powerState('off')

    print('Measurement complete, closing file.')
    outputFile.closeFile()



#Check for 20uA current compliance, step down if hit.
#At end of ramp up loop, take 3 measurements 10s apart prior to stepping down.
#Active area of R1 sensor: 89.06 cm^2, R4: 87.45 cm^2 -> current threshold R1: 8.906 uA, R4: 8.745 uA @ 20C
#Scale for measured temp.

def normaliseCurrent(current, temp):
    normCurrent = current * (293/(273+temp))**2 * numpy.exp(-1.2/(2*8.617E-5)*(1/293 - 1/(273+temp)))
    return normCurrent 