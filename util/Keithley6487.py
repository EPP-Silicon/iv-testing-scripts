from util.deviceUtilities import deviceUtilities

class Keithley6487(deviceUtilities):

    def __init__(self):
        device_id = "GPIB0::22::INSTR" #This should never change
        super().__init__(device_id)

    def setupDevice(self):
        self.open()
        self.sendCommand("FUNC \'CURR\'")
        self.sendCommand("ARM:SOUR IMM")
        self.sendCommand("ARM:COUN 1")

        self.sendCommand("SYST:ZCH ON")
        self.sendCommand("CURR:RANG 2e-9")  
        self.sendCommand("INIT")  
        self.sendCommand("SYST:ZCOR:STAT OFF")
#        self.sendCommand("NPLC 1")
        self.sendCommand("SYST:ZCH:ACQ")
        self.sendCommand("SYST:ZCOR ON")
        self.sendCommand("SYST:ZCH OFF")
        self.sendCommand("CURR:RANG 2e-5")

        self.sendCommand("SENS:AVER:COUN 5")
        self.sendCommand("SENS:AVER:STAT ON")
        self.sendCommand("FORM:ELEM READ,VSO")
#        self.sendCommand("*OPC?") #Query if operations complete

        print("Device initialised, preparing for measurement")

    def readCurrent(self):
    #    self.sendCommand("N0X")
    #    self.sendCommand("N1B1X")
        current = self.getData()
#string splitting here to remove other info
        current = current.split(",")[0]        
        return current

