import sys, json
from argparse import ArgumentParser
from measurement.currentVoltage import performIV



if __name__ == "__main__":

   parser = ArgumentParser()
   parser.add_argument('settings', type=str, help='configuration settings for the run: (module, sensor, tabbed-sensor, test)')
   args = parser.parse_args()



   with open('util/config.json','r') as config_file:
      config = json.load(config_file)
   selectedConfig = config[args.settings]

   performIV(args.settings, selectedConfig)