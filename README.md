# IV testing scripts

Python scripts inteded for the the automation of I-V testing sensors and modules for the ITk construction programme at Melbourne.
Script controls the voltage ramp up (and ramp down) of a Kethley 2260B-800 PSU with periodic current measurements with Keithley 6487.
Environmental conditions are captured every 0.5s by a BME 680 sensor and called from local influxdb at the start of each run.  
  
Dependancies:  
pyvisa  
influxdb  
gpib-linux (gpib to usb via Keysight 82375B)  
  
Run parameters are defined in util/config.json  
  
Usage:  python3 startMeasurement.py \<run type\>  
run type should match a set of run parameters as listed in the cofig file.
