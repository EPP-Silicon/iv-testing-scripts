import influxdb

def openDBConnection():

    influx_connection = influxdb.InfluxDBClient(host='45.113.234.152', port=80, path='/influx', timeout=60, retries=10)
    influx_connection.switch_database('sensor_logs_weather')
    return influx_connection

def queryDB():
    influx_connection = openDBConnection()
    humidityValue = 0
    temperatureValue = 0
    databaseName = 'bme_environment'
    result = influx_connection.query('SELECT humidity, temperature FROM {} WHERE sensor_id = \'UNIT-03\' GROUP BY * ORDER BY DESC LIMIT 1'.format(databaseName))

    for k,v in result.items():
        for p in v:
            humidityValue = p['humidity']
            temperatureValue = p['temperature']

    return temperatureValue, humidityValue
