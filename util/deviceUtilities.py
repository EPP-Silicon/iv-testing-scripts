import time, sys
import pyvisa

class deviceUtilities:

   def __init__(self,device_id):
      self.device_id = device_id
      self.rm = None
#      self.response = None

   def open(self):
      self.rm = pyvisa.ResourceManager('@py').open_resource(self.device_id)
      
   def close(self):
      self.rm.close()
      self.rm = None

   def sendCommand(self, command, wait=0.1):
      self.rm.write(command)
      time.sleep(wait)

   def getData(self):
#      self.sendCommand("READ?")
      response = self.rm.query("READ?")
      return response