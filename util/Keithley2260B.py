from util.deviceUtilities import deviceUtilities

class Keithley2260B(deviceUtilities):

    def __init__(self):
        device_id = "ASRL/dev/ttyACM0::INSTR" #Does this change?
        super().__init__(device_id)

    def setupDevice(self):
        self.open()
        self.sendCommand("SOUR:CURR:LEV:IMM:AMPL 0.1")
        self.sendCommand("SOUR:VOLT:LEV:IMM:AMPL 0.0")

    def setVoltage(self, voltage):
        self.sendCommand("SOUR:VOLT:LEV:IMM:AMPL " + str(voltage))

    def powerState(self, onOff):
        if onOff == "on":
            onOff = 1
        if onOff == "off":
            onOff = 0
        self.sendCommand("OUTP:STAT:IMM " + str(onOff))